package br.com.trama.navigationdrawer;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class BlankFragment extends Fragment {

    private int param;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (arguments != null && arguments.containsKey(MainActivity.ARG_PARAM)) {
            param = arguments.getInt(MainActivity.ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        ImageView img = (ImageView) view.findViewById(R.id.icon_drawer_row);
        TextView textView = (TextView) view.findViewById(R.id.title_drawer_row);

        Resources resources = getResources();

        TypedArray typedArray = resources.obtainTypedArray(R.array.drawer_icons);

        String[] stringArray = resources.getStringArray(R.array.drawer_itens);

        img.setImageResource(typedArray.getResourceId(param, 0));
        textView.setText(stringArray[param]);

        typedArray.recycle();

        return view;
    }
}
