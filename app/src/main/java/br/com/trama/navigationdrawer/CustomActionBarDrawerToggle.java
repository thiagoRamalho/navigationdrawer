package br.com.trama.navigationdrawer;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * Created by thiago on 26/02/15.
 */
@SuppressWarnings("DefaultFileTemplate")
class CustomActionBarDrawerToggle extends ActionBarDrawerToggle {


    private final ActionBarActivity activity;

    public CustomActionBarDrawerToggle(ActionBarActivity activity, DrawerLayout drawerLayout, Toolbar toolBar) {
        super(activity, drawerLayout, toolBar, R.string.drawer_open, R.string.drawer_close);
        this.activity = activity;
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
        activity.invalidateOptionsMenu();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
        activity.invalidateOptionsMenu();
    }
}
