package br.com.trama.navigationdrawer;

/**
 * Created by thiago on 27/02/15.
 */
@SuppressWarnings("DefaultFileTemplate")
public class DrawerItem {

    private final int imageRId;
    private final String title;

    public DrawerItem(int imageRId, String title) {
        this.imageRId = imageRId;
        this.title = title;
    }

    public int getImageRId() {
        return imageRId;
    }

    public String getTitle() {
        return title;
    }
}
