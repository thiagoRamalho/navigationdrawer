package br.com.trama.navigationdrawer;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;


public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.DrawerItemClickCallback {


    public static final String ARG_PARAM = "ARG_PARAM";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolBar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolBar);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        FragmentManager supportFragmentManager = getSupportFragmentManager();

        NavigationDrawerFragment navFragment =
                (NavigationDrawerFragment) supportFragmentManager.findFragmentById(R.id.navigation_drawer_fragment);

        navFragment.setUp(drawerLayout, toolBar);

        onItemClicked(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onItemClicked(int position) {

        Bundle bundle = new Bundle();
        bundle.putInt(MainActivity.ARG_PARAM, position);

        BlankFragment blankFragment = new BlankFragment();
        blankFragment.setArguments(bundle);

        FragmentManager supportFragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.container, blankFragment);

        fragmentTransaction.commit();
    }
}
