package br.com.trama.navigationdrawer;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class NavigationDrawerFragment extends Fragment {

    private DrawerLayout drawerLayout;
    private ListView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        recyclerView = (ListView) layout.findViewById(R.id.drawer_list);

        createDrawerListItens();

//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//        recyclerView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int childPosition = recyclerView.getChildPosition(v);
//
//                Toast.makeText(NavigationDrawerFragment.this.getActivity(), "Click " + childPosition + " position", Toast.LENGTH_SHORT).show();
//            }
//        });

//

        recyclerView.setOnItemClickListener(new MyOnItemClickListener());

        return layout;
    }

    private void createDrawerListItens() {

        Resources res = getResources();

        TypedArray typedArray = res.obtainTypedArray(R.array.drawer_icons);

        String[] stringArray = res.getStringArray(R.array.drawer_itens);

        @SuppressWarnings("Convert2Diamond") List<DrawerItem> list = new ArrayList<DrawerItem>();

        for (int i = 0; i < typedArray.length(); i++) {
            list.add(new DrawerItem(typedArray.getResourceId(i, 0), stringArray[i]));
        }

        typedArray.recycle();

        DrawerAdapter adapter = new DrawerAdapter(this.getActivity(), list);

        recyclerView.setAdapter(adapter);
    }

    public void setUp(final DrawerLayout drawerLayout, Toolbar toolBar) {
        this.drawerLayout = drawerLayout;

        CustomActionBarDrawerToggle actionBarDrawerToggle = new CustomActionBarDrawerToggle((ActionBarActivity) getActivity(),
                drawerLayout, toolBar);

        this.drawerLayout.setDrawerListener(actionBarDrawerToggle);

        this.drawerLayout.post(new MyRunnable(actionBarDrawerToggle));
    }

    interface DrawerItemClickCallback {
        public void onItemClicked(int rId);
    }

    private class MyRunnable implements Runnable {

        private final ActionBarDrawerToggle actionBarDrawerToggle;

        private MyRunnable(ActionBarDrawerToggle actionBarDrawerToggle) {
            this.actionBarDrawerToggle = actionBarDrawerToggle;
        }

        @Override
        public void run() {
            this.actionBarDrawerToggle.syncState();
        }
    }

    private class MyOnItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            drawerLayout.closeDrawers();

            DrawerItemClickCallback callback = (DrawerItemClickCallback) getActivity();

            callback.onItemClicked(position);
        }
    }
}
